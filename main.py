from flask import Flask, render_template, flash, redirect, url_for, session, logging, request
from wtforms import Form, StringField, TextAreaField, PasswordField, validators
from data import article
from passlib.hash import sha256_crypt
from flask_mysqldb import MySQL


app = Flask(__name__)
mysql = MySQL(app)
@app.route('/')
def index():
    return render_template('home.html')

Articles = article()
@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/articles')
def getArticles():
    return render_template('articles.html', articles = Articles)

@app.route('/article/<string:id>/')
def getArticle(id):
    return render_template('article.html', id=id)

class RegisterForm(Form):
    name = StringField('Name',[validators.Length(min=1,max=50)])
    username = StringField('Username',[validators.Length(min=4,max=25)])
    email = StringField('Email',[validators.Length(min=6,max=50)])
    password = PasswordField('Password', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message="Password do not match")
    ])
    confirm = PasswordField('Confirm Passowrd')
@app.route('register',method=['GET','POST'])
def register():
    form = RegisterForm(request.form)

    if request.method =='POST' and form.validate():

        return render_template('register.html',form)

if __name__ == '__main__':
    app.run()