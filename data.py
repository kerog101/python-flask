def article():
    articles =[
        {
            'id':1,
            'title':'Article 1',
            'body': 'Test body 1',
            'author':'Brad Ducan'
        },
        {
            'id': 2,
            'title': 'Article 2',
            'body': 'Test body 2',
            'author':'Bob Builder'
        }
    ]

    return articles